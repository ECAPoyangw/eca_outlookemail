﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrmMailSave2019
{
    public partial class MemberSelectForm : Form
    {
        // A list to contain the member companies that were selected by the user or the email address
        // of the sender/recipient was recognised as a contact for these member companies
        List<MemberListVm> SelectedMemberTags = new List<MemberListVm>();
        BindingSource bs = new BindingSource();

        public MemberSelectForm(string message = null, string[] param = null)
        {
            InitializeComponent();

            // Set the label message
            label3.Text = message;



            // Populate the drop-down list with Message Categories
            List<MessageCategoryVm> Categories = MessageCategories();
            cmbMessageCategory.DataSource = Categories;
            cmbMessageCategory.DisplayMember = "ValueName"; // Set the list property that is displayed


            // Populate the list box with items
            lstMemberList.DataSource = MemberFilter(null);
            lstMemberList.DisplayMember = "CompanyName"; // Set the list property that is displayed     


            bs.DataSource = SelectedMemberTags;

            // Add the CompanyIds in param array (if any) to the list of selected member tags
            if (param != null)
            {
                foreach (string p in param)
                {
                    var recognisedContacts = lstMemberList.Items.Cast<MemberListVm>().Where(x => x.ID == p).ToList<MemberListVm>();
                    if (recognisedContacts != null)
                    {
                        foreach (MemberListVm item in recognisedContacts)
                        {
                            bs.Add(new MemberListVm()
                            {
                                CompanyName = item.CompanyName,
                                ID = item.ID,
                                Table = item.Table
                            });
                        }
                    }
                }
            }

            lstMemberSelect.DataSource = bs;



        }

        // Filter the list of results based on the searchTerm
        private List<MemberListVm> MemberFilter(string searchTerm)
        {
            List<MemberListVm> MembersList = AllMembers();
            List<MemberListVm> MembersListFilter;

            // If the searchTerm contains a value then filter the list by searchTerm, otherwise return the full list
            if (searchTerm != null)
            {
                MembersListFilter = MembersList.Where(member => member.CompanyName.ToString().ToLower().Contains(searchTerm)).ToList();
            }
            else
            {
                MembersListFilter = AllMembers();
            }

            return MembersListFilter;
        }

        // Build the initial list containing all of the categories
        private List<MessageCategoryVm> MessageCategories()
        {

            DbConnection conn = new DbConnection();

            string sqlCmd = "procMem_CrmMailSave";

            var sqlParams = new object[] {
                new { name = "@iAppMode", value = 3 }
            };

            var sqlresult = conn.SqlSpResult<MessageCategoryVm>(sqlCmd, sqlParams);

            return sqlresult.ToList();
        }

        // Build the initial list containing all of the members
        private List<MemberListVm> AllMembers()
        {

            DbConnection conn = new DbConnection();

            string sqlCmd = "procMem_CrmMailSave";

            var sqlParams = new object[] {
                new { name = "@iAppMode", value = 2 }
            };

            var sqlresult = conn.SqlSpResult<MemberListVm>(sqlCmd, sqlParams);

            return sqlresult.ToList();
        }


        private void EncryptConnString()
        {

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSection section = config.GetSection("connectionStrings");
            if (!section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                config.Save();
            }
        }

        // When a member is selected push the rest of the controls down/up to prevent having a scroll bar on lstMemberSelect
        private void ResizeMemberSelectList()
        {
            int listOldSize = lstMemberSelect.Size.Height;
            int listNewSize = 0;
            int listResize = 0;
            int itemHeight = lstMemberSelect.ItemHeight;
            int itemCount = lstMemberSelect.Items.Count;
            // Expand the height of the MemberSelect listbox
            lstMemberSelect.Height = itemHeight * itemCount;

            // Set the new size of the MemberSelect listbox
            listNewSize = lstMemberSelect.Size.Height;
            listResize = listNewSize - listOldSize;

            // Push the other controls up/down the page
            ControlCollection controls = (ControlCollection)this.Controls;

            foreach (Control control in controls)
            {
                if (control.Location.Y >= lstMemberSelect.Location.Y && control.Name != "lstMemberSelect")
                {
                    control.Location = new Point(control.Location.X, control.Location.Y + listResize);
                }
            }
        }



        // ****** CONTROL EVENTS ****** //

        // Filter the list when the text is changed
        private void txbMemberFilter_TextChanged(object sender, EventArgs e)
        {
            lstMemberList.DataSource = MemberFilter(txbMemberFilter.Text.ToLower());
        }

        private void lstMemberList_DoubleClick(object sender, EventArgs e)
        {
            // Add the item to the MemberSelect list
            if (SelectedMemberTags.Contains((MemberListVm)lstMemberList.SelectedItem) == false)
            {
                //lstMemberSelect.Items.Add(lstMemberList.SelectedItem);
                SelectedMemberTags.Add((MemberListVm)lstMemberList.SelectedItem);
                bs.ResetBindings(false);
            }

            // Resize the MemberSelect listbox
            ResizeMemberSelectList();
        }

        private void lstMemberSelect_DoubleClick(object sender, EventArgs e)
        {
            SelectedMemberTags.Remove((MemberListVm)lstMemberSelect.SelectedItem);
            bs.ResetBindings(false);
            // Resize the MemberSelect listbox
            ResizeMemberSelectList();
        }

        private void lstMemberSelect_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();

            Color color = this.BackColor;

            bool isItemSelected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);
            int itemIndex = e.Index;
            if (itemIndex >= 0 && itemIndex < lstMemberSelect.Items.Count)
            {
                Graphics g = e.Graphics;

                // Background colour
                SolidBrush backgroundColorBrush = new SolidBrush((isItemSelected) ? (Color)color : (Color)color);
                g.FillRectangle(backgroundColorBrush, e.Bounds);

                // Set text colour
                MemberListVm itemText = (MemberListVm)lstMemberSelect.Items[itemIndex];

                SolidBrush itemTextColorBrush = (isItemSelected) ? new SolidBrush(Color.Black) : new SolidBrush(Color.Black);
                g.DrawString(itemText.CompanyName, e.Font, itemTextColorBrush, lstMemberSelect.GetItemRectangle(itemIndex).Location);

                // Clean up
                backgroundColorBrush.Dispose();
                itemTextColorBrush.Dispose();
            }
            e.DrawFocusRectangle();
        }

        private void submit_Click(object sender, EventArgs e)
        {
            string MemberTags = "";
            string MessageCategory = "";
            string separator = ",";

            // Disable the submit button to prevent further clicks
            //this.submit.Enabled = false;

            // Make sure the user has selected a MessageCategory
            MessageCategoryVm selectedCategory = (MessageCategoryVm)cmbMessageCategory.SelectedItem;

            if (selectedCategory.NoteCategoryId == "0")
            {
                MessageCategory = "";
            }
            else
            {
                MessageCategory = selectedCategory.NoteCategoryId;
            }


            // Concatenate the MemberTags into a string
            foreach (MemberListVm item in lstMemberSelect.Items)
            {
                MemberTags += item.Table + "_" + item.ID.ToString() + separator;
            }

            // Remove the final separator character from the end of the string
            if (MemberTags != "" && MemberTags.Substring(MemberTags.Length - 1) == separator)
            {
                MemberTags = MemberTags.TrimEnd(separator.ToCharArray());
            }

            // Attempt to upload the data again
            if (MemberTags != "" && MessageCategory != "")
            {
                // Disable the submit button to prevent further clicks
                this.submit.Enabled = false;

                MailHandler mHandler = new MailHandler();
                mHandler.DataCollector(MemberTags, MessageCategory);

                // Close the form after uploading
                this.Close();
            }


        }

        private void cancel_Click(object sender, EventArgs e)
        {
            // Close the form
            this.Close();
        }
    }
}
