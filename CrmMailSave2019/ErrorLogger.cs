﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;



namespace CrmMailSave2019
{
    public static class ErrorLogger
    {

        public static string installDir = ConfigurationManager.AppSettings["InstallDir"];
        public static string errorlogfile = "CrmMailSave_ErrorLog.txt";
        public static string username = WindowsIdentity.GetCurrent().Name;
        public static bool goodConnection = true;

        public static void Log(string error)
        {
            string logdate = DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss");
            var fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ECAErrorLogger.txt");

            try
            {


                using (StreamWriter sw = new StreamWriter(fileName, true))
                {

                    sw.WriteLine("[" + logdate + " - " + username + "] ");
                    sw.WriteLine("[ " + error + " ]");


                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("ErrorMsg: " + ex.Message.ToString() + "\n" +
                                "ErrorStackTrace: " + ex.StackTrace.ToString());
            }
        }

    }
}
