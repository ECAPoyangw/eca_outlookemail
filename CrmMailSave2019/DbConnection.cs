﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrmMailSave2019
{
    class DbConnection
    {
        // This class returns a list of rows returned from a stored procedure, using late binding to map the C# model to the database fields


        public static int[] errorNumbers = { -1 };

        // DEVSQL01 database:
        //private string connStr = "data source=DEVSQL01\\ECADEV2008;initial catalog=ECACrmCms;Trusted_Connection=true; MultipleActiveResultSets=True";
        //private string connStr = ConfigurationManager.ConnectionStrings["EcaCRM"].ConnectionString;

        private string connStr = "Server=tcp:ecaproddbfailover.database.windows.net;Initial Catalog=eca-prod-dbs-Crm;Persist Security Info=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;User ID=IUsr_Connect;Password=(WhatTheFarkIsthepoxyAzurepwdpolicy1!)";
        //private string connStr = "Server=tcp:eca-uat-westeurope.database.windows.net,1433;Initial Catalog=eca-uat-dbs-ECACRM;Persist Security Info=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;User ID=IUsr_Connect;Password=(WhatTheFarkIsthepoxyAzurepwdpolicy1!)";

        public List<T> SqlSpResult<T>(string sqlCmd, object[] sqlParams)
        {
            // sqlCmd: The name of the Sql SP to execute
            // sqlParams: Array of objects containing parameter name and value used in the SP
            List<T> Results = new List<T>();
            Type objType = typeof(T);

            using (SqlConnection conn = new SqlConnection(connStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(sqlCmd))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        // Give up after trying to get data for ... seconds
                        cmd.CommandTimeout = 30;

                        // Add a new sql Parameter for each combination of parameter name/value pair in sqlParams
                        if (sqlParams != null && sqlParams.Length > 0)
                        {
                            foreach (var p in sqlParams)
                            {
                                var name = p.GetType().GetProperty("name").GetValue(p, null);
                                var value = p.GetType().GetProperty("value").GetValue(p, null);
                                cmd.Parameters.Add(new SqlParameter(name.ToString(), value.ToString()));
                            }
                        }

                        conn.Open();
                        cmd.Connection = conn;


                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            try
                            {
                                while (rdr.Read())
                                {
                                    // Create a dynamic object of the specified type to contain the values for each record found
                                    dynamic listObj = Activator.CreateInstance(typeof(T));

                                    // Check the properties for specified type
                                    var properties = listObj.GetType().GetProperties();

                                    // Get the names of the fields so we can exclude the properties for which there is no field value
                                    var columns = Enumerable.Range(0, rdr.FieldCount).Select(rdr.GetName).ToList();

                                    // Set the value of the dynamic object property to the value returned from Stored Proc
                                    foreach (var prop in properties)
                                    {
                                        if (columns.Contains(prop.Name))
                                        {
                                            PropertyInfo propInfo = listObj.GetType().GetProperty(prop.Name);
                                            propInfo.SetValue(listObj, rdr.GetValue(rdr.GetOrdinal(prop.Name)), null);
                                        }
                                    }
                                    // Return a list containing the dynamic objects
                                    Results.Add(listObj);
                                }
                            }
                            catch (SqlException ex)
                            {


                                // If the error number is in errorNumbers array then set goodConnection = false, to prevent reconnecting to DB
                                if (Array.IndexOf(errorNumbers, ex.Number) > 0)
                                {
                                    ErrorLogger.goodConnection = false;
                                }

                                ErrorLogger.Log("ErrorMsg: " + ex.Message.ToString() + "\n" +
                                                "ErrorStackTrace: " + ex.StackTrace.ToString());
                                MessageBox.Show("An error occurred while reading table in db. Please try again later, or contact IT Support " + ex.Message.ToString());
                                return null;
                            }

                        }
                    }
                }
                catch (SqlException ex)
                {



                    // If the error number is in errorNumbers array then set goodConnection = false, to prevent reconnecting to DB
                    if (Array.IndexOf(errorNumbers, ex.Number) > -1)
                    {
                        ErrorLogger.goodConnection = false;
                    }


                    ErrorLogger.Log("ErrorMsg: " + ex.Message.ToString() + "\n" +
                                    "ErrorStackTrace: " + ex.StackTrace.ToString());
                    MessageBox.Show("An error occurred while connecting to the server. Please try again later, or contact IT Support " + ex.Message.ToString());
                    return null;
                }
                // Reset the cursor back to the default...
                Cursor.Current = Cursors.Default;
                return Results;
            }

        }
    }



    //class DbConnectionToCorDocument
    //{
    //    // This class returns a list of rows returned from a stored procedure, using late binding to map the C# model to the database fields


    //    public static int[] errorNumbers = { -1 };

    //    // DEVSQL01 database:
    //    //connectionString="Persist Security Info=False;Data Source=eca-uat-westeurope.database.windows.net;Initial Catalog=eca-uat-dbs-CorDocument;User ID=IUser_Finance;Password=ECA_B!tc0!n;Encrypt=True;TrustServerCertificate=False" />

    //    private string connStr = "Server=tcp:ecaproddbfailover.database.windows.net;Initial Catalog=eca-prod-dbs-CorDocument;Persist Security Info=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;User ID=IUsr_Connect;Password=(WhatTheFarkIsthepoxyAzurepwdpolicy1!)";
    //    //private string connStr = ConfigurationManager.ConnectionStrings["EcaCRM"].ConnectionString;
    //    //private string connStr = "Server=tcp:eca-uat-westeurope.database.windows.net,1433;Initial Catalog=eca-uat-dbs-CorDocument;Persist Security Info=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;User ID=IUsr_Connect;Password=(WhatTheFarkIsthepoxyAzurepwdpolicy1!)";


    //    public int SqlSpResult(string sqlCmd, object[] sqlParams)
    //    {
    //        // sqlCmd: The name of the Sql SP to execute
    //        // sqlParams: Array of objects containing parameter name and value used in the SP
    //        //List<T> Results = new List<T>();
    //        //Type objType = typeof(T);

    //        using (SqlConnection conn = new SqlConnection(connStr))
    //        {
    //            try
    //            {
    //                using (SqlCommand cmd = new SqlCommand(sqlCmd))
    //                {
    //                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

    //                    // Give up after trying to get data for ... seconds
    //                    cmd.CommandTimeout = 30;

    //                    // Add a new sql Parameter for each combination of parameter name/value pair in sqlParams
    //                    if (sqlParams != null && sqlParams.Length > 0)
    //                    {
    //                        foreach (var p in sqlParams)
    //                        {
    //                            var name = p.GetType().GetProperty("name").GetValue(p, null);
    //                            var value = p.GetType().GetProperty("value").GetValue(p, null);
    //                            cmd.Parameters.Add(new SqlParameter(name.ToString(), value.ToString()));
    //                        }
    //                    }

    //                    conn.Open();
    //                    cmd.Connection = conn;
    //                    int result = cmd.ExecuteNonQuery();
                        

    //                    return result;
    //                    //using (SqlDataReader rdr = cmd.ExecuteReader())
    //                    //{
    //                    //    try
    //                    //    {
    //                    //        while (rdr.Read())
    //                    //        {
    //                    //            // Create a dynamic object of the specified type to contain the values for each record found
    //                    //            dynamic listObj = Activator.CreateInstance(typeof(T));

    //                    //            // Check the properties for specified type
    //                    //            var properties = listObj.GetType().GetProperties();

    //                    //            // Get the names of the fields so we can exclude the properties for which there is no field value
    //                    //            var columns = Enumerable.Range(0, rdr.FieldCount).Select(rdr.GetName).ToList();

    //                    //            // Set the value of the dynamic object property to the value returned from Stored Proc
    //                    //            foreach (var prop in properties)
    //                    //            {
    //                    //                if (columns.Contains(prop.Name))
    //                    //                {
    //                    //                    PropertyInfo propInfo = listObj.GetType().GetProperty(prop.Name);
    //                    //                    propInfo.SetValue(listObj, rdr.GetValue(rdr.GetOrdinal(prop.Name)), null);
    //                    //                }
    //                    //            }
    //                    //            // Return a list containing the dynamic objects
    //                    //            Results.Add(listObj);
    //                    //        }
    //                    //    }
    //                    //    catch (SqlException ex)
    //                    //    {


    //                    //        // If the error number is in errorNumbers array then set goodConnection = false, to prevent reconnecting to DB
    //                    //        if (Array.IndexOf(errorNumbers, ex.Number) > 0)
    //                    //        {
    //                    //            ErrorLogger.goodConnection = false;
    //                    //        }

    //                    //        ErrorLogger.Log("ErrorMsg: " + ex.Message.ToString() + "\n" +
    //                    //                        "ErrorStackTrace: " + ex.StackTrace.ToString());
    //                    //        MessageBox.Show("An error occurred while communicating with the server. Please try again later, or contact IT Support " + ex.Message.ToString());
    //                    //        return null;
    //                    //    }

    //                    //}
    //                }
    //            }
    //            catch (SqlException ex)
    //            {



    //                // If the error number is in errorNumbers array then set goodConnection = false, to prevent reconnecting to DB
    //                if (Array.IndexOf(errorNumbers, ex.Number) > -1)
    //                {
    //                    ErrorLogger.goodConnection = false;
    //                }


    //                ErrorLogger.Log("ErrorMsg: " + ex.Message.ToString() + "\n" +
    //                                "ErrorStackTrace: " + ex.StackTrace.ToString());
    //                MessageBox.Show("An error occurred while connecting to the cordocument db. Please try again later, or contact IT Support " + ex.Message.ToString());
    //                return 0;
    //            }
    //            // Reset the cursor back to the default...
    //            Cursor.Current = Cursors.Default;
    //            //return res;
    //        }

    //    }
    //}

}
